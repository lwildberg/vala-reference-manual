Vala Reference Manual
=====================

This is the reference for all features, syntax elements, internals and generated output of Vala.

It is not the best place to learn about Vala and especially for beginners might be not easy to understand.
For learning and tutorials and for beginners the `Vala Documentation <https://lwildberg.pages.gitlab.gnome.org/vala-tutorial>`__ is a better place.

To contribute to this reference you can submit merge request to the `Gitlab repository <https://gitlab.gnome.org/lwildberg/vala-reference-manual>`__.

.. toctree::
   :maxdepth: 2

   overview
   concepts
   types
   expressions
   statements
   namespaces
   methods
   delegates
   errors
   classes
   interfaces
   generics
   structs
   enumerated-types-(enums)
   attributes
   preprocessor
   gir-metadata-format
   gidl-metadata-format
