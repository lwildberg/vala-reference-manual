GIDL metadata format
====================

This section describes the format of .metadata files as used by
*vapigen* as additional information for .vapi file generation. Some of
the information specified in the metadata can be used to set :doc:`attributes`
as well.

Comments
--------

Comments start with a ``#`` and end at the end of a line. For example:

::

   # this is a comment

Other Lines
-----------

Every non-comment line in the file is made of up two sections: the
specifier, and the parameters.

The specifier is the first text to appear on the line, and it specifies
what the rest of the line will be modifying.

The parameters are a space separated list of a parameter name, followed
by an equals sign and the value enclosed in quotes.

For example, this line sets *parameter1* and *parameter2* on *foo.bar*:

::

   foo.bar parameter1="value" parameter2="value"

Specifiers
----------

Specifiers always use the C name for whatever it is you are modifying.
For example if your namespace is *Foo*, and the Vala name for the type
is *Bar*, then you would use ``FooBar``.

Specifiers may also use wildcards, and all items that partially match
the specifier will be selected. For example:

::

   *.klass hidden="1"

will hide the *klass* field in all types.

Specifying Different Things
---------------------------

To specify a:

==================== ===========================
Function             ``name_of_function``
Type                 ``Type``
Property             ``Type:property_name``
Signal               ``Type::signal_name``
Field                ``Type.field_name``
Parameter (Function) ``name_of_function.param``
Parameter (Delegate) ``DelegateName.param``
Parameter (Signal)   ``Type::signal_name.param``
==================== ===========================

For example, hiding a symbol:

======== ============================
Type     ``Foo hidden="1"``
Function ``some_function hidden="1"``
Field    ``Foo.bar hidden="1"``
======== ============================

Properties Reference
--------------------

The format for the entries will be like so

+------------+-----------------+-----------------+-----------------+
| **Name**   | **Applies To**  | **Values**      | **Description** |
+------------+-----------------+-----------------+-----------------+
| ``foobar`` | Signal,         | The acceptable  | The description |
|            | Function,       | values          | goes here.      |
|            | Class, Struct,  |                 |                 |
|            | etc             |                 |                 |
+------------+-----------------+-----------------+-----------------+

And in alphabetical order:

+----------------+----------------+----------------+----------------+
| **Name**       | **Applies To** | **Values**     | *              |
|                |                |                | *Description** |
+----------------+----------------+----------------+----------------+
| ``abstract``   | Class,         | 0, 1           |                |
|                | Function       |                |                |
+----------------+----------------+----------------+----------------+
| ``acc          | Property       | 0, 1           |                |
| essor_method`` |                |                |                |
+----------------+----------------+----------------+----------------+
| ``array_       | Field          | C identifier   |                |
| length_cname`` |                |                |                |
+----------------+----------------+----------------+----------------+
| ``arra         | Parameter      | Double         | Sets the       |
| y_length_pos`` | (Function)     | (position      | position of    |
|                |                | between two    | the length for |
|                |                | Vala           | the parameter, |
|                |                | parameters)    | length needs   |
|                |                |                | to be hidden   |
|                |                |                | separately.    |
+----------------+----------------+----------------+----------------+
| ``array        | Parameter      | C type         |                |
| _length_type`` | (Function),    |                |                |
|                | Function       |                |                |
|                | (returning an  |                |                |
|                | array), Field  |                |                |
+----------------+----------------+----------------+----------------+
| ``array_nul    | Function       | 0, 1           |                |
| l_terminated`` | (returning an  |                |                |
|                | array),        |                |                |
|                | Parameter      |                |                |
|                | (Function),    |                |                |
|                | Field          |                |                |
+----------------+----------------+----------------+----------------+
| ``async``      | Function       | 0, 1           | Force async    |
|                |                |                | function, even |
|                |                |                | if it doesn't  |
|                |                |                | end in \_async |
+----------------+----------------+----------------+----------------+
| ``base_class`` | Class          | C type         | Marks the base |
|                |                |                | class for the  |
|                |                |                | type           |
+----------------+----------------+----------------+----------------+
| ``base_type``  | Struct         | Vala type      | Marks the      |
|                |                |                | struct as      |
|                |                |                | inheriting     |
+----------------+----------------+----------------+----------------+
| ``chea         | Anything       | Header include | Compiler will  |
| der_filename`` | (except        | path           | adds the       |
|                | parameters)    |                | specified      |
|                |                |                | header when    |
|                |                |                | thing is used. |
+----------------+----------------+----------------+----------------+
| ``c            | Enum           | String         | Removes a      |
| ommon_prefix`` |                |                | common prefix  |
|                |                |                | from           |
|                |                |                | enumeration    |
|                |                |                | values         |
+----------------+----------------+----------------+----------------+
| `              | Class          | C type         |                |
| `const_cname`` | (non-GObject)  |                |                |
+----------------+----------------+----------------+----------------+
| ``c            | Class          | C function     |                |
| opy_function`` | (non-GObject)  | name           |                |
+----------------+----------------+----------------+----------------+
| ``cprefix``    | Module         | String         |                |
+----------------+----------------+----------------+----------------+
| ``ctype``      | Parameter      | C type         |                |
|                | (Function),    |                |                |
|                | Field          |                |                |
+----------------+----------------+----------------+----------------+
| ``d            | Parameter      | Any Vala value | Sets the       |
| efault_value`` | (Function)     | that would be  | default value  |
|                |                | valid for the  | for a          |
|                |                | type           | parameter.     |
+----------------+----------------+----------------+----------------+
| ``delegat      | Parameter      | Double         |                |
| e_target_pos`` | (Function)     | (position      |                |
|                |                | between two    |                |
|                |                | Vala           |                |
|                |                | parameters)    |                |
+----------------+----------------+----------------+----------------+
| ``deprecated`` | Anything       | 0, 1           | Marks the      |
|                | (except        |                | thing as       |
|                | parameters)    |                | deprecated     |
+----------------+----------------+----------------+----------------+
| ``depr         | Anything       | Version        | Marks the      |
| ecated_since`` | (except        |                | thing as       |
|                | parameters)    |                | deprecated     |
+----------------+----------------+----------------+----------------+
| ``ellipsis``   | Function       | 0, 1           | Marks that the |
|                |                |                | function has a |
|                |                |                | variable       |
|                |                |                | argument list  |
+----------------+----------------+----------------+----------------+
| `              | Enum           | 0, 1           | Marks the      |
| `errordomain`` |                |                | enumeration as |
|                |                |                | a GError       |
|                |                |                | domain         |
+----------------+----------------+----------------+----------------+
| `              | Function       | C function     | Sets custom    |
| `finish_name`` |                | name           | asynchronous   |
|                |                |                | finish         |
|                |                |                | function       |
+----------------+----------------+----------------+----------------+
| ``f            | Class          | C function     | Sets a free    |
| ree_function`` | (non-GObject)  | name           | function for   |
|                |                |                | the struct     |
+----------------+----------------+----------------+----------------+
| ``g            | Module         | String         |                |
| ir_namespace`` |                |                |                |
+----------------+----------------+----------------+----------------+
| `              | Module         | Version        |                |
| `gir_version`` |                |                |                |
+----------------+----------------+----------------+----------------+
| ``has_c        | Struct         | 0, 1           | marks the      |
| opy_function`` |                |                | struct as      |
|                |                |                | having a copy  |
|                |                |                | function       |
+----------------+----------------+----------------+----------------+
| ``has_dest     | Struct         | 0, 1           |                |
| roy_function`` |                |                |                |
+----------------+----------------+----------------+----------------+
| `              | Signal         | 0, 1           |                |
| `has_emitter`` |                |                |                |
+----------------+----------------+----------------+----------------+
| ``has_target`` | Delegate       | 0, 1           |                |
+----------------+----------------+----------------+----------------+
| `              | Class, Enum,   | 0, 1           | Marks whether  |
| `has_type_id`` | Struct         |                | a GType is     |
|                |                |                | registered for |
|                |                |                | this thing     |
+----------------+----------------+----------------+----------------+
| ``hidden``     | Anything       | 0, 1           | Causes the     |
|                |                |                | selected thing |
|                |                |                | to not be      |
|                |                |                | output in the  |
|                |                |                | vapi file.     |
+----------------+----------------+----------------+----------------+
| ``immutable``  | Struct         | 0, 1           | Marks the      |
|                |                |                | struct as      |
|                |                |                | immutable      |
+----------------+----------------+----------------+----------------+
| ``             | Delegate       | Double         |                |
| instance_pos`` |                | (Position      |                |
|                |                | between two    |                |
|                |                | Vala           |                |
|                |                | parameters)    |                |
+----------------+----------------+----------------+----------------+
| ``is_array``   | Function       | 0, 1           | Marks the      |
|                | (returning an  |                | thing as an    |
|                | array),        |                | array          |
|                | Parameter,     |                |                |
|                | Field          |                |                |
+----------------+----------------+----------------+----------------+
| ``is           | Class          | 0, 1           |                |
| _fundamental`` | (non-GObject)  |                |                |
+----------------+----------------+----------------+----------------+
| ``             | Class          | 0, 1           |                |
| is_immutable`` | (non-GObject)  |                |                |
+----------------+----------------+----------------+----------------+
| ``is_out``     | Parameter      | 0, 1           | Marks the      |
|                |                |                | parameter as   |
|                |                |                | "out"          |
+----------------+----------------+----------------+----------------+
| ``is_ref``     | Parameter      | 0, 1           | Marks the      |
|                |                |                | parameter as   |
|                |                |                | "ref"          |
+----------------+----------------+----------------+----------------+
| ``i            | Struct, Union  | 0, 1           | Marks type as  |
| s_value_type`` |                |                | a value type   |
|                |                |                | (aka struct)   |
+----------------+----------------+----------------+----------------+
| ``lower_       | Module         | String         |                |
| case_cprefix`` |                |                |                |
+----------------+----------------+----------------+----------------+
| ``lower_       | Interface      | String         |                |
| case_csuffix`` |                |                |                |
+----------------+----------------+----------------+----------------+
| ``name``       | Any Type,      | Vala           | Changes the    |
|                | Function,      | identifier     | name of the    |
|                | Signal         |                | thing, does    |
|                |                |                | not change     |
|                |                |                | namespace      |
+----------------+----------------+----------------+----------------+
| ``namespace``  | Any Type       | String         | Changes the    |
|                |                |                | namespace of   |
|                |                |                | the thing      |
+----------------+----------------+----------------+----------------+
| ``na           | Signal         | String         | Specify the    |
| mespace_name`` | Parameter      |                | namespace of   |
|                |                |                | the parameter  |
|                |                |                | type indicated |
|                |                |                | with type_name |
+----------------+----------------+----------------+----------------+
| ``no_          | Function       | 0, 1           | Does not       |
| array_length`` | (returning an  |                | implicitly     |
|                | array),        |                | pass/return    |
|                | Parameter      |                | array length   |
|                | (Function,     |                | to/from        |
|                | Delegate)      |                | function       |
+----------------+----------------+----------------+----------------+
| ``nullable``   | Function       | 0, 1           | Marks the      |
|                | (having a      |                | value as       |
|                | return value), |                | nullable       |
|                | Parameter      |                |                |
+----------------+----------------+----------------+----------------+
| ``owned_get``  | Property       | 0, 1           |                |
+----------------+----------------+----------------+----------------+
| ``parent``     | Any module     | String         | Strip          |
|                | member         | (Namespace)    | namespace      |
|                |                |                | prefix from    |
|                |                |                | symbol and put |
|                |                |                | it into given  |
|                |                |                | sub-namespace  |
+----------------+----------------+----------------+----------------+
| ``p            | Function       | 0, 1           |                |
| rintf_format`` |                |                |                |
+----------------+----------------+----------------+----------------+
| ``rank``       | Struct         | Integer        |                |
+----------------+----------------+----------------+----------------+
| ``             | Class          | C function     |                |
| ref_function`` | (non-GObject)  | name           |                |
+----------------+----------------+----------------+----------------+
| ``ref_f        | Class          | 0, 1           |                |
| unction_void`` | (non-GObject)  |                |                |
+----------------+----------------+----------------+----------------+
| ``rename_to``  | Any Type       | Vala           | Renames the    |
|                |                | identifier     | type to        |
|                |                |                | something      |
|                |                |                | else, ie       |
|                |                |                | fooFloat to    |
|                |                |                | float (not     |
|                |                |                | exactly the    |
|                |                |                | same as        |
|                |                |                | ``name``,      |
|                |                |                | AFAIK name     |
|                |                |                | changes both   |
|                |                |                | the vala name  |
|                |                |                | and the cname. |
|                |                |                | rename_to adds |
|                |                |                | the required   |
|                |                |                | code so that   |
|                |                |                | when the       |
|                |                |                | rename_to'ed   |
|                |                |                | type is used,  |
|                |                |                | the c type is  |
|                |                |                | used)          |
+----------------+----------------+----------------+----------------+
| `              | Anything       | The thing that | Specifies a    |
| `replacement`` | (except        | replaces this  | replacement    |
|                | parameters)    |                | for a          |
|                |                |                | deprecated     |
|                |                |                | symbol         |
+----------------+----------------+----------------+----------------+
| ``sentinel``   | Function (with | C value        | The sentinel   |
|                | ellipsis)      |                | value marking  |
|                |                |                | the end of the |
|                |                |                | vararg list    |
+----------------+----------------+----------------+----------------+
| `              | Struct         | 0, 1           | Marks the      |
| `simple_type`` |                |                | struct as      |
|                |                |                | being a simple |
|                |                |                | type, like int |
+----------------+----------------+----------------+----------------+
| ``tak          | Parameter      | 0, 1           |                |
| es_ownership`` | (Function,     |                |                |
|                | Delegate)      |                |                |
+----------------+----------------+----------------+----------------+
| ``throws``     | Function       | 0, 1           | Marks that the |
|                |                |                | function       |
|                |                |                | should use an  |
|                |                |                | out parameter  |
|                |                |                | instead of     |
|                |                |                | throwing an    |
|                |                |                | error          |
+----------------+----------------+----------------+----------------+
| ``to_string``  | Enum           | C function     |                |
|                |                | name           |                |
+----------------+----------------+----------------+----------------+
| ``transf       | Function/D     | 0, 1           | Transfers      |
| er_ownership`` | elegate/Signal |                | ownership of   |
|                | (having a      |                | the value      |
|                | return value), |                |                |
|                | Parameter      |                |                |
|                | (Function,     |                |                |
|                | Signal)        |                |                |
+----------------+----------------+----------------+----------------+
| ``ty           | Function/D     | Vala types,    | Restricts the  |
| pe_arguments`` | elegate/Signal | comma          | generic type   |
|                | (having a      | separated      | of the thing   |
|                | return value), |                |                |
|                | Property,      |                |                |
|                | Field,         |                |                |
|                | Parameter      |                |                |
+----------------+----------------+----------------+----------------+
| ``type_ch      | Class          | C              |                |
| eck_function`` | (GObject)      | function/macro |                |
|                |                | name           |                |
+----------------+----------------+----------------+----------------+
| ``type_id``    | Struct, Class  | C macro        |                |
|                | (GObject)      |                |                |
+----------------+----------------+----------------+----------------+
| ``type_name``  | Function       | Vala type name | Changes the    |
|                | (having a      |                | type of the    |
|                | return value), |                | selected       |
|                | Property,      |                | thing.         |
|                | Parameter,     |                | Overwrites old |
|                | Field          |                | type, so       |
|                |                |                | "type_name"    |
|                |                |                | must be before |
|                |                |                | any other type |
|                |                |                | modifying      |
|                |                |                | metadata       |
+----------------+----------------+----------------+----------------+
| ``typ          | Delegate,      | Vala generic   |                |
| e_parameters`` | Class          | type           |                |
|                | (non-GObject)  | parameters,    |                |
|                |                | e.g. T, comma  |                |
|                |                | separated      |                |
+----------------+----------------+----------------+----------------+
| ``un           | Class          | C function     |                |
| ref_function`` | (non-GObject)  | name           |                |
+----------------+----------------+----------------+----------------+
| `              | Parameter      | 0, 1           |                |
| `value_owned`` | (Function)     |                |                |
+----------------+----------------+----------------+----------------+
| ``vfunc_name`` | Function       | C function     |                |
|                |                | pointer name   |                |
+----------------+----------------+----------------+----------------+
| ``virtual``    | Function       | 0, 1           |                |
+----------------+----------------+----------------+----------------+
| ``weak``       | Field          | 0, 1           | Marks the      |
|                |                |                | field as weak  |
+----------------+----------------+----------------+----------------+

Examples
--------

Demonstrating...

::

   // ...
