# Vala Reference Manual

The deployed pages can be viewed [here](https://lwildberg.pages.gitlab.gnome.org/vala-reference-manual)

To build it locally run ```make``` in the source directory. The generated html files are stored in ```build/html```.
